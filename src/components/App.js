import React, { Component } from 'react';
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import ReactLoading from 'react-loading';
import '../styles/App.css';
import { getPokemonById, getAll, removeFromPokedex, addToPokedex, getPokedex } from '../utils/services';

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      number: 0,
      art_url: ''
    };
  }

  handleClick = () => {
    // removeFromPokedex(index)
    // .then(res => {
    //   //setState
    //   console.log('APP>', res);
    // }).catch(err => {
    //   console.log('ERR>', err)
    // });
    
    // addToPokedex(id)
    //   .then(res => {
    //     //setState
    //     console.log('APP>', res);
    //   }).catch(err => {
    //     console.log('ERR>', err)
    //   });

    // getPokedex()
    //   .then(res => {
    //     //setState
    //     console.log('APP>', res);
    //   }).catch(err => {
    //     console.log('ERR>', err)
    //   });
    
    //   getAll().then(res => {
    //   // setState
    //   console.log('APP>', res);
    // }).catch(err => {
    //   console.log('ERR>', err)
    // });
    let number = this.state.number;

    getPokemonById(number)
      .then(res => {
        console.log('Res/Back at app:', res);
        this.setState({
          name: res.data.name,
          number: res.data.national_id,
          description: res.data.description,
          art_url: res.data.art_url
        });
      })
      .catch(err => {
        console.error('Err/Back to app:', err);
      });
  };

  handleChange = (e) =>{
    this.setState({
      number: e.target.value
    })
    
  }

  render() {
    return (
      <div className='app-outer'>
        <Container style={{ backgroundColor: 'yellow' }}>
          <Row>
            <Col>
            HeLLo
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col sm={6} style={{ backgroundColor: 'pink' }}>
              tHiS iS pOkEmOn
            </Col>
            <Col sm={6} style={{ backgroundColor: '#61C85F' }}>
              yeeee
            </Col>
          </Row>
          <Row style={{backgroundColor: 'gray'}}>
            <Col md={{ span: 6, offset: 3 }}>
            {/* <img src={this.state.art_url} alt=" no image "></img> */}
            <input onChange={this.handleChange} placeholder='Enter a number'></input>
              {/* <Form>
                <Form.Group controlId='formBasicEmail'>
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type='email' placeholder='Enter email' />
                  <Form.Text className='text-muted'>
                    We'll always share your email with everyone else.
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId='formBasicPassword'>
                  <Form.Label>Password</Form.Label>
                  <Form.Control type='password' placeholder='Password' />
                </Form.Group>
                <Form.Group controlId='formBasicChecbox'>
                  <Form.Check type='checkbox' label='Check me out' />
                </Form.Group>
                <Button variant='primary' type='submit'>
                  Submit
                </Button>
              </Form> */}
            </Col>
          </Row>
          <br></br>
        </Container>
        <Button variant='primary' onClick={this.handleClick}>
          Get Pokemon
        </Button>
        <Button variant= 'primary' onClick= {this.handleClick}>
          Load it up fam 
        </Button>
        <img src={this.state.art_url} alt=" no image ">
        </img>
        <br></br>
        <div style={{ textAlign: 'center' }}>
          <div style={{ display: 'inline-block' }}>
            <ReactLoading
              type={'bars'}
              color={'blue'}
              height={20}
              width={20}
            />
          </div>
        </div>
        <p>State:&nbsp;{JSON.stringify(this.state)}</p>
      </div>
    );
  }
}

export default App;
