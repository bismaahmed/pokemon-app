import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';
// import { shallow } from '../../setupTests';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('<App />', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders children when passed in', () => {
    const wrapper = Enzyme.shallow(
      <App>
        <div className='unique' />
      </App>
    );
    expect(wrapper.contains(<div className='unique' />)).toEqual(true);
  });
});
