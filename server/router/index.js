const router = require('express').Router();
const fs = require('fs');
const { pokemonData } = require('../db/data');

router.route('/getPokemonById/:id').get((req, res) => {
  let run = Math.floor(Math.random() * 7);
  let { id } = req.params;
  let pokemon = pokemonData[id - 1];
  return new Promise((resolve, reject) => {
    if (!run) {
      reject(res.status(500).json('Try again later'));
    } else {
      setTimeout(() => {
        resolve(res.json(pokemon));
      }, 2500);
    }
  });
});

router.route('/getAll').get((req, res) => {
  res.json(pokemonData);
});

router.route('/addToPokedex/:id').get((req, res) => {
  let { id } = req.params;
  let pokemon = pokemonData[id - 1];
  return new Promise((resolve, reject) => {
    fs.readFile('./server/db/pokedex.json', 'utf-8', (err, buf) => {
      if (err) {
        console.log('Error Reading', err);
        reject(err);
      }
      let pokedex = JSON.parse(buf);
      pokedex.data.push(pokemon);
      fs.writeFile(
        './server/db/pokedex.json',
        JSON.stringify(pokedex),
        (err, buf) => {
          if (err) {
            console.log('Error Wrinting:', err);
            reject(err);
          }
          resolve(res.send(pokedex));
        }
      );
    });
  });
});

router.route('/removeFromPokedex/:index').get((req, res) => {
  let { index } = req.params;
  return new Promise((resolve, reject) => {
    fs.readFile('./server/db/pokedex.json', 'utf-8', (err, buf) => {
      if (err) {
        console.log('Error Reading', err);
        reject(res.send(err));
      }
      let pokedex = JSON.parse(buf);
      pokedex.data.splice(index, 1);
      fs.writeFile(
        './server/db/pokedex.json',
        JSON.stringify(pokedex),
        (err, buf) => {
          if (err) {
            console.log('Error Wrinting:', err);
            reject(res.send(err));
          }
          resolve(res.send(pokedex));
        }
      );
    });
  });
});

router.route('/getPokedex').get((req, res) => {
  return new Promise((resolve, reject) => {
    fs.readFile('./server/db/pokedex.json', 'utf-8', (err, buf) => {
      if (err) {
        console.log('Error Reading:', err);
        reject(res.send(err));
      }
      let pokedex = JSON.parse(buf);
      resolve(res.send(pokedex));
    });
  });
});

module.exports = router;
