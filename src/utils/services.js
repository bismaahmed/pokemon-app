import axios from 'axios';

/**
 * Endpoints:
 * /getPokemonById/{id} - returns object of pokemon with same id
 * /getAll - returns array of all 151 pokemon
 * /getPokedex - returns object with data array of pokemon added to it.
 * /addToPokedex/{id} - adds pokemon to pokedex
 * /removeFromPokedex/{index} - removes pokemon from pokemon data array at specific index.
 * 
 */

export function getPokemonById(id) {
  console.log('Making a call with:', id);
  return axios
    .get(`http://localhost:8080/getPokemonById/${id}`)
    .then(res => {
      console.log('util/Response:', res);
      return res;
    })
    .catch(err => {
      console.error('util/Error:', err);
      throw new Error(err);
    });
}

// export function getAll() {
//   return axios
//     .get('http://localhost:8080/getAll')
//     .then(res => {
//       console.log('RESPONSE FROM SERVER>', res);
//       return res
//     })
//       .catch(err => {
//       console.log('Error retriving', err)
//       return err
//   })
// }

// export function getPokedex() {
//   return axios
//     .get('http://localhost:8080/getPokedex')
//     .then(res => {
//       console.log('RESPONSE FROM SERVER>', res);
//       return res
//     })
//       .catch(err => {
//       console.log('Error retriving', err)
//       return err
//   })
// }

// export function addToPokedex(id) {
//   return axios
//   .get(`http://localhost:8080/addToPokedex/${id}`)
//     .then(res => {
//       console.log('RESPONSE FROM SERVER>', res);
//       return res
//   }).catch(err => {
//     console.log('Error retriving', err)
//     return err
//   })
// }

// export function removeFromPokedex(index) {
//   return axios.get(`http://localhost:8080/removeFromPokedex/${index}`)
//   .then(res => {
//     console.log('RESPONSE FROM SERVER>', res);
//     return res
//   }).catch(err => {
//     console.log('Error retriving', err)
//     return err
//   })
//}